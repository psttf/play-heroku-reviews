name := "play-heroku-reviews"
organization := "com.tylip"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.4"

resolvers += Resolver.jcenterRepo

resolvers += "Sonatype snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/"

val playSlickVersion = "3.0.3"

libraryDependencies ++= Seq(
  "org.scala-lang" % "scala-compiler" % scalaVersion.value,
  "com.softwaremill.macwire" %% "macros" % "2.3.0" % "provided",
  "org.postgresql" % "postgresql" % "42.1.4",
  "com.typesafe.play" %% "play-slick" % playSlickVersion,
  "com.liyaos" %% "scala-forklift-slick" % "0.3.1",
  ehcache,
  filters,
)

scalacOptions ++= Seq(
  "-deprecation", // Emit warning and location for usages of deprecated APIs.
  "-feature", // Emit warning and location for usages of features that should be imported explicitly.
  "-unchecked", // Enable additional warnings where generated code depends on assumptions.
//  "-Xfatal-warnings", // Fail the compilation if there are any warnings.
  //"-Xlint", // Enable recommended additional warnings.
  "-Ywarn-adapted-args", // Warn if an argument list is modified to match the receiver.
  "-Ywarn-dead-code", // Warn when dead code is identified.
  "-Ywarn-inaccessible", // Warn about inaccessible types in method signatures.
  "-Ywarn-nullary-override", // Warn when non-nullary overrides nullary, e.g. def foo() over def foo.
//  "-Ywarn-numeric-widen", // Warn when numerics are widened.
  // Play has a lot of issues with unused imports and unsued params
  // https://github.com/playframework/playframework/issues/6690
  // https://github.com/playframework/twirl/issues/105
  "-Xlint:-unused,_",
  "-Ypartial-unification"
)

herokuBuildpacks in Compile := Seq(
  "heroku/metrics",
  "heroku/jvm"
)

sources in (Compile, doc) := Seq.empty
publishArtifact in (Compile, packageDoc) := false

PlayKeys.devSettings ++= Seq(
  "play.server.http.idleTimeout" -> "60s",
  "play.server.akka.requestTimeout" -> "40s"
)

val herokuApiKey =
  taskKey[String]("check that heroku app exists")
val herokuAppExists =
  taskKey[Boolean]("check that heroku app exists")
val herokuAppCreate =
  taskKey[Boolean]("create heroku app")
val herokuAppDelete =
  taskKey[Boolean]("delete heroku app")
val herokuDbAttachmentExists =
  taskKey[Boolean]("check that db is attached to heroku app")
val herokuDbAttachmentCreate =
  taskKey[Boolean]("attach db to heroku app")
val herokuConfigVarsExist =
  taskKey[Boolean]("check that db is attached to heroku app")
val herokuConfigVarsCreate =
  taskKey[Boolean]("attach db to heroku app")

herokuApiKey := {
  System.getenv("HEROKU_API_KEY")
}

herokuAppExists := {
  import dispatch._, Defaults._
  import scala.concurrent.Await
  import scala.concurrent.duration._
  val appName = (Compile / herokuAppName).value
  val svc = url(
    s"https://api.heroku.com/apps/$appName"
  )
    .addHeader("Accept", "application/vnd.heroku+json; version=3")
    .addHeader("Authorization", s"Bearer ${herokuApiKey.value}")
    .GET
  val result =
    Await.result(Http.default(svc) map (_.getStatusCode == 200), 30.seconds)
  println(s"$appName Heroku app exists: $result")
  result
}

herokuAppCreate := {
  import dispatch._, Defaults._
  import scala.concurrent.Await
  import scala.concurrent.duration._
  val apiKey = herokuApiKey.value
  val appName = (Compile / herokuAppName).value
  val result =
    if (!herokuAppExists.value) {
      val svc = url("https://api.heroku.com/apps")
        .addHeader("Accept", "application/vnd.heroku+json; version=3")
        .addHeader("Authorization", s"Bearer $apiKey")
        .addHeader("Content-Type", "application/json")
        .setStringBody(s"""{"name": "$appName"}""")
        .POST
      Await.result(Http.default(svc) map ( _.getStatusCode == 201 ), 30.seconds)
    } else false
  println(s"$appName Heroku app created: $result")
  result
}

herokuAppDelete := {
  import dispatch._, Defaults._
  import scala.concurrent.Await
  import scala.concurrent.duration._
  val apiKey = herokuApiKey.value
  val appName = (Compile / herokuAppName).value
  val result =
    if (herokuAppExists.value) {
      val svc = url(
        s"https://api.heroku.com/apps/$appName"
      )
        .addHeader("Accept", "application/vnd.heroku+json; version=3")
        .addHeader("Authorization", s"Bearer $apiKey")
        .DELETE
      Await.result(Http.default(svc) map ( _.getStatusCode == 200 ), 30.seconds)
    } else false
  println(s"$appName Heroku app deleted: $result")
  result
}

val dbId = "postgresql-sinuous-12928"

herokuDbAttachmentExists := {
  import dispatch._, Defaults._
  import scala.concurrent.Await
  import scala.concurrent.duration._
  val appName = (Compile / herokuAppName).value
  val svc = url(s"https://api.heroku.com/apps/$appName/addon-attachments/$dbId")
    .addHeader("Accept", "application/vnd.heroku+json; version=3")
    .addHeader("Authorization", s"Bearer ${herokuApiKey.value}")
    .GET
  val result =
    Await.result(Http.default(svc) map (_.getStatusCode == 200), 30.seconds)
  println(s"$appName Heroku app DB attachment exists: $result")
  result
}

herokuDbAttachmentCreate := {
  import dispatch._, Defaults._
  import scala.concurrent.Await
  import scala.concurrent.duration._
  val apiKey = herokuApiKey.value
  val appName = (Compile / herokuAppName).value
  val result =
    if (herokuAppExists.value) {
      val svc = url("https://api.heroku.com/addon-attachments")
        .addHeader("Accept", "application/vnd.heroku+json; version=3")
        .addHeader("Authorization", s"Bearer $apiKey")
        .addHeader("Content-Type", "application/json")
        .setStringBody(
          s"""{
             |  "app": "$appName",
             |  "addon": "$dbId",
             |  "name": "DATABASE"
             |}""".stripMargin)
        .POST
      Await.result(Http.default(svc) map ( _.getStatusCode == 201 ), 30.seconds)
    } else false
  println(s"$appName Heroku app DB attachment created: $result")
  result
}

herokuConfigVarsExist := {
  import dispatch._, Defaults._
  import scala.concurrent.Await
  import scala.concurrent.duration._
  val appName = (Compile / herokuAppName).value
  val svc = url(s"https://api.heroku.com/apps/$appName/config-vars")
    .addHeader("Accept", "application/vnd.heroku+json; version=3")
    .addHeader("Authorization", s"Bearer ${herokuApiKey.value}")
    .GET
  val result =
    Await.result(Http.default(svc) map (
      _.getResponseBody.contains("PLAY_HTTP_SECRET")
    ), 30.seconds)
  println(s"$appName Heroku app config vars exist: $result")
  result
}

herokuConfigVarsCreate := {
  import dispatch._, Defaults._
  import scala.concurrent.Await
  import scala.concurrent.duration._
  val apiKey = herokuApiKey.value
  val appName = (Compile / herokuAppName).value
  val secret = PlayKeys.generateSecret.value
  val result =
    if (herokuAppExists.value) {
      val svc = url(s"https://api.heroku.com/apps/$appName/config-vars")
        .addHeader("Accept", "application/vnd.heroku+json; version=3")
        .addHeader("Authorization", s"Bearer $apiKey")
        .addHeader("Content-Type", "application/json")
        .setStringBody(
          s"""{
             |  "PLAY_HTTP_SECRET": "$secret"
             |}""".stripMargin)
        .PATCH
      Await.result(Http.default(svc) map ( _.getStatusCode == 200 ), 30.seconds)
    } else false
  println(s"$appName Heroku app config vars created: $result")
  result
}
