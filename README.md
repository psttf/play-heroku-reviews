Scala Play Framework Example of Review Apps Setup for GitLab and Heroku
=======================================================================

The `deploy_stage` CI task allows to manually create a review app on Heroku for
each branch. It is reasonable to create them when reviewing merge requests. In
fact, it is required to create a review app for a merge request since a
successful pipeline is required for merging. In this model review apps serve as
staging environments. All review apps share the same database since they are
expected to be created when all the changes in the corresponding branch are
tested locally and ready to be deployed to the staging environment. In turn, all
the commits to `master` initiate a production deployment. Thus, merging a merge
request is treated as promoting a staging version to production. Only 
fast-forward merges are allowed in order to avoid unexpected errors.
