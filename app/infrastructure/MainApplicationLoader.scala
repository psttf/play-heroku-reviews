package infrastructure

import java.time.ZoneOffset
import java.util.{Locale, TimeZone}

import com.softwaremill.macwire._
import controllers._
import play.api.ApplicationLoader.Context
import play.api.{ApplicationLoader, BuiltInComponentsFromContext, LoggerConfigurator}
import play.filters.csrf.CSRFComponents
import play.filters.headers.SecurityHeadersComponents
import play.filters.hosts.AllowedHostsComponents
import play.filters.https.RedirectHttpsComponents

import scala.concurrent.Await
import scala.concurrent.duration._

final class MainApplicationLoader extends ApplicationLoader {
  def load(context: Context) = {
    LoggerConfigurator(context.environment.classLoader).foreach {
      _.configure(context.environment, context.initialConfiguration, Map.empty)
    }
    new MainComponents(context).application
  }
}

//noinspection ScalaUnusedSymbol
final class MainComponents(context: Context)
extends BuiltInComponentsFromContext(context)
with CSRFComponents
with SecurityHeadersComponents
with AllowedHostsComponents
with RedirectHttpsComponents
with AssetsComponents
with DatabaseComponents
with MigrationComponents {

  TimeZone.setDefault(TimeZone.getTimeZone(ZoneOffset.UTC))
  Locale.setDefault(Locale.US)

  Await.result(migrator.initIfNeededAsync, 30.seconds)
  migrator.up

  private implicit lazy val implicitAssetsFinder = assetsFinder

  lazy val config = configuration.underlying

  lazy val httpFilters = Seq()

  private lazy val helloController = wire[HelloController]

  private lazy val routePrefix = "/"
  lazy val router = wire[_root_.router.Routes]

}
