package infrastructure

import play.api.db.slick.{DbName, SlickComponents}
import slick.basic.DatabaseConfig
import slick.jdbc.{JdbcBackend, JdbcProfile}

trait DatabaseComponents extends SlickComponents {
  protected lazy val dbConfig: DatabaseConfig[JdbcProfile] =
    slickApi.dbConfig(DbName("default"))
  implicit lazy val db: JdbcBackend#DatabaseDef =
    dbConfig.db
}
