package infrastructure

import com.softwaremill.macwire.wire
import data.db.migrations.Migrator
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

trait MigrationComponents {

  protected def dbConfig: DatabaseConfig[JdbcProfile]

  val migrator = wire[Migrator]

//  migrator.migrations = data.db.migrations.all

}
