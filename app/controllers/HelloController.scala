package controllers

import play.api.mvc.{BaseController, ControllerComponents}

final class HelloController(
  val controllerComponents: ControllerComponents
) extends BaseController {
  def index = Action { implicit request => Ok("hello-2") }
}
