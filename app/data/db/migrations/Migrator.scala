package data.db.migrations

import com.liyaos.forklift.slick.SlickMigrationManager
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile
import slick.jdbc.meta.MTable

import scala.concurrent.{ExecutionContext, Future}

// TODO: DRY migrator

final class Migrator(
  dbConfigParam: => DatabaseConfig[JdbcProfile]
) extends SlickMigrationManager {

  override lazy val dbConfig = dbConfigParam

  import dbConfig.profile.api._

  def initializedAsync(implicit ec: ExecutionContext) =
    db.run(MTable.getTables(migrationsTable.baseTableRow.tableName))
      .map(_.nonEmpty)

  def initIfNeededAsync(implicit ec: ExecutionContext) =
    initializedAsync flatMap {
      case true => Future.successful(())
      case false => db run migrationsTable.schema.create
    }

}
