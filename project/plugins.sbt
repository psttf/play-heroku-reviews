libraryDependencies ++= Seq(
  "org.dispatchhttp" %% "dispatch-core" % "0.14.0"
)

addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.6.11")

addSbtPlugin("com.heroku" % "sbt-heroku" % "2.1.1")
